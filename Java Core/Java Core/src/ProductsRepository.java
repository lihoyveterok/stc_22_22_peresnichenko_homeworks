import java.util.List;

public interface ProductsRepository {

    List<Product> findById(Integer id);
}
