import java.io.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        ProductsRepository productsRepository = new ProductsRepositoryFileBasedImpl("input.txt");
        System.out.println(productsRepository.findById(2));

    }
}