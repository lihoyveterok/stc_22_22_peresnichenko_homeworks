import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ATM atm = new ATM(500, 100, 1000);



        Scanner scanner = new Scanner(System.in);
        int amountFromConsole = scanner.nextInt();


        int myMoney = 0;

        int result = atm.giveMoney(amountFromConsole);
        if (result != -1) {
            myMoney += result;
            System.out.println("мои деньги " + myMoney);
            System.out.println("В банкомате осталось " + atm.currentAmountInsideAtm);
        } else {
            System.out.println("денег нет");
        }

        int amountToPut = scanner.nextInt();
        int cashBack = atm.putMoney(amountToPut);
        if (cashBack == -1) {
            myMoney -= amountToPut;
            System.out.println("осталось " + myMoney);
        } else {
            System.out.println("сдача " + cashBack);
        }

        System.out.println("Число операций равно " + atm.sumOperation );
    }
}