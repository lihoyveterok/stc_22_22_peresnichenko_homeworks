
public class ATM {

   public int currentAmountInsideAtm;
    int maxCashGiveMoney;
    int maxCashInATM;
    int sumOperation = 0;


    ATM(int initialSumLastMoney, int maxGiveMoney, int maxInATM) {
        maxCashGiveMoney = maxGiveMoney;
        maxCashInATM = maxInATM;
        currentAmountInsideAtm = initialSumLastMoney;
    }

    void operationDone() {
        sumOperation += 1;
    }

    public int giveMoney(int requestedAmount) {
        if (requestedAmount <= maxCashGiveMoney && requestedAmount <= currentAmountInsideAtm) {
            currentAmountInsideAtm = currentAmountInsideAtm - requestedAmount;
            operationDone();
            return requestedAmount;
        }
        else return - 1;
    }

    public int putMoney(int amount) {
        int sum = currentAmountInsideAtm + amount;
        operationDone();
         if (sum <= maxCashInATM) {
             currentAmountInsideAtm += amount;
             return 0;
         }
         else {
             int cashBack = sum - maxCashInATM;
             currentAmountInsideAtm += amount - cashBack;
             return cashBack;
         }


    }
}