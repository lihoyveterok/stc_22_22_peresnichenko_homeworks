import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            int newEl = scanner.nextInt();
            array[i] = newEl;
        }


        int count = 0;

        if (array.length == 1) count = 1;
        else {
            if (array[0] < array[1]) count++;

            if (array[size - 1] < array[size - 2]) count++;

            for (int i = 1; i < size - 1; i++) {
                if (array[i - 1] > array[i] && array[i + 1] > array[i]) count++;
            }
        }


        System.out.println(count);
        System.out.println(Arrays.toString(array));

    }
}