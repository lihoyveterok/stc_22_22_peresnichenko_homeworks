public class Main {
    public static void main(String[] args){
        Square square = new Square(3);
        Rectangle rectangle = new Rectangle(5,10);
        Circle circle = new Circle(4);
        Ellipse ellipse = new Ellipse(6, 3);



        rectangle.getArea();
        square.getArea();
        circle.getArea();
        ellipse.getArea();

        square.getPerimeter();
        rectangle.getPerimeter();
        circle.getPerimeter();
        ellipse.getPerimeter();
        }

    }

