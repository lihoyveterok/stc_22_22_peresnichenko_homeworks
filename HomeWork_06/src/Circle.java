public class Circle extends Figure{

    public Circle(double radius){
        super();
        this.radius = radius;
    }

    public Circle() {

    }


    public void getPerimeter(){
        Perimeter = 2 * Math.PI * radius;
        System.out.println("Периметр круга " + Perimeter);
    }
    public void getArea() {
        Area = Math.PI * (radius * radius);
        System.out.println(" Площадь круга " + Area);
    }
}
