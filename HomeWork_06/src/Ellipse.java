public class Ellipse extends Circle {


    public Ellipse(double bigRadius, double smallRadius) {
        super();
        this.bigRadius = bigRadius;
        this.smallRadius = smallRadius;
    }

    public void getPerimeter(){
        Perimeter =  4 * (Math.PI + (bigRadius - smallRadius) * 2) / bigRadius + smallRadius;
        System.out.println("Периметр Эллипса " + Perimeter);
    }
    public void getArea(){
        Area = Math.PI * bigRadius * smallRadius;
        System.out.println("Площадь эллипла " + Area);
    }
}
