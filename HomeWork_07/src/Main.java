public class Main {

    public static void completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }


    public static void main(String[] args) {
        Task a1 = new EvenNumbersPrintTask(1, 9);
        Task a2 = new OddNumbersPrintTask(12, 17);

        Task[] tasks1 = {a1, a2};
        completeAllTasks(tasks1);

    }

}
