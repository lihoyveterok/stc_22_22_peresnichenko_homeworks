
abstract class AbstractNumbersPrintTask implements Task {

    int from;
    int to;

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public abstract void complete();


}

