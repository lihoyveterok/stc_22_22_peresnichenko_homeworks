public class Main {
    public static void main(String[] args) {

        // Посчитать сумму элементов массива в заданном промежутке
        ArrayTask arrayTask1 = (array, from, to) -> {

            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            };
            return sum;

        }; System.out.print("Сумма элементов массива  ");


        ArrayTask arrayTask2 = (array, from, to) -> {

            int max = array[from];
            for (int i = from; i < to; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            int num = max;
            int sum = 0;
                while (num > 0) {
                    sum += num % 10;
                    num /= 10;
                }System.out.print("Сумма элементов массива  ");
                return sum;

            };

            int[] array = {11, 12, 44, 14, 6, 16,};

            ArraysTasksResolver ATR = new ArraysTasksResolver();

            ATR.resolveTask(array, arrayTask1,0 ,1);
            ATR.resolveTask(array, arrayTask2, 1 ,5);
        };

    }