import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.function.Function;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {

        this.fileName = fileName;
    }


    private static final Function<String, Product> productsMapper = currentProducts -> {
        String[] parts = currentProducts.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        double price = Double.parseDouble(parts[2]);
        Integer amount = Integer.parseInt(parts[3]);
        return new Product(id, name, price, amount);
    };

    private static final Function<Product, String> productsToStringMapper = products ->
            products.getId() + "|" + products.getName() + "|" + products.getPrice() + "|" + products.getAmount();


    @Override
    public Product findById(Integer id) {
        try (Reader fileReader = new FileReader("imput.txt");
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader.lines().map(productsMapper).filter(product -> product.getId().equals(id)).findFirst().orElse(null);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}






