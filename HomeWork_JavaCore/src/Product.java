public class Product {

        private Integer id;
        private String name;
        private double price;
        private Integer amount;

        public Product(Integer id, String name, double price, Integer amount) {
            this.id = id;
            this.name = name;
            this.price = price;
            this.amount = amount;
        }


        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public Integer getAmount() {
            return amount;
        }

        @Override
        public String toString() {
            return "Products{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", price=" + price +
                    ", amount=" + amount +
                    '}';
        }
    }

