create table driver
(
    id              bigserial primary key,
    first_name      varchar(25),
    last_name       varchar(25),
    phone_number    varchar(20) unique,
    experience      integer
        check (experience >= 0 and experience <= 70),
    age             integer
        check (age >= 0 and age <= 100),
    driving_license bool not null,
    category        varchar(2),
    rating          decimal
        check (rating >= 0 and rating <= 5.00)
);

create table car
(
    id           serial primary key,
    car_type     varchar(15) not null,
    color        varchar(10) default 'UNKNOWN',
    car_number   varchar(10) unique,
    id_car_owner bigint   not null,
    foreign key (id_car_owner) references driver (id)
);

create table ride
(
    car_id       bigint,
    foreign key (car_id) references car (id),
    driver_id    bigint,
    foreign key (driver_id) references driver (id),
    journey_date timestamp,
    start_time   time,
    finish_time  time
)
